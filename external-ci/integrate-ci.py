from builder import builder
import platform
import experiment
import argparse
import time
import shutil
import os

def main( args ):

  # Initialize the platform instance
  this_platform = platform.supported_platforms[args.platform](
    args.canesm_source,
    args.num_atmosphere_nodes,
    args.num_ocean_ranks
  )

  # Parse and initialize the requested experiments to run
  requested_experiments = args.experiments.split(' ')
  experiments = [ experiment.experiments[exp](args.cache_dir, this_platform) for exp in requested_experiments ]

  # Copy the builds to a cached directory
  start = time.time()

  for exp in experiments:
    start_exp = time.time()
    exp.setup_run_directory(this_platform)
    exp.configure()
    exp.integrate(this_platform,args.num_atmosphere_nodes,args.num_ocean_ranks)
    exp.test_regression(this_platform)
    print(f'{exp.name} completed in {time.time()-start_exp:.2f} seconds')

  print('---')
  print(f'All experiments completed in: {time.time()-start:.2f} seconds')

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Handles the build stage of the Gitlab CI')
  parser.add_argument('cache_dir', help='Path to the Gitlab CI cache')
  parser.add_argument('canesm_source', help='The path where the CanESM source code is located')
  parser.add_argument('platform', help='The identifier for the platform to run on')
  parser.add_argument('experiments', help='Experiment names to run')
  parser.add_argument('num_atmosphere_nodes', help='Number of atmosphere nodes')
  parser.add_argument('num_ocean_ranks', help='Number of ocean MPI ranks')

  args = parser.parse_args()
  main(args)