git references
==============

Basic git references
--------------------
- `Official git documentation <https://git-scm.com/doc>`_
- `Atlassian git tutorials <https://www.atlassian.com/git/tutorials>`_
- `Git - submodule docs <https://git-scm.com/book/en/v2/Git-Tools-Submodules>`_
- `Git - working with remotes <https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes>`_
- `Atlassian - git merge conflicts <https://www.atlassian.com/git/tutorials/using-branches/merge-conflicts>`_
- `Github - resolving merge conflicts from the command line <https://docs.github.com/en/free-pro-team@latest/github/collaborating-with-issues-and-pull-requests/resolving-a-merge-conflict-using-the-command-line>`_


Git submodule references
------------------------
- `Official git submodule documentation <https://git-scm.com/book/en/v2/Git-Tools-Submodules>`_
- `Atlassian git submodule documentation <https://www.atlassian.com/git/tutorials/git-submodule>`_
- `Using submodules in git - tutorial <https://www.vogella.com/tutorials/GitSubmodules/article.html>`_
- `Working with submodules - blog <https://github.blog/2016-02-01-working-with-submodules/>`_
- :ref:`CCCma generic submodule overview <Submodules - generic>`

Forking workflow references
---------------------------
- `Atlassian - Forking Workflows <https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow>`_
- `Medium - Git Forking Workflow <https://medium.com/dev-genius/git-forking-workflow-bbba0226d39c>`_
- :ref:`CCCma generic forking overview <Forking - generic>`