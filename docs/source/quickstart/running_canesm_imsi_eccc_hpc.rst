******************************************************** 
 Running CanESM on ECCC U2 using imsi
********************************************************

Overview and resources
######################

This document provides instructions for running the CanESM5 model on the
ECCC U2 system. This guide is relying on a model configuration system in 
development called *imsi*, which is in alpha testing to replace config-canesm.
You should use the ppp's for imsi work (for now).

To get a pre-installed, stable version of imsi, and some required modules:

.. code-block:: bash

    source /home/scrd102/cccma_libs/imsi/imsi_cccma_v0.3.0/bin/activate

You may also install imsi yourself (see `imsi repo <https://gitlab.com/cccma/imsi>`_).
Read the `imsi documentation <https://imsi.readthedocs.io/en/latest/?version=latest>`_ to get an
overview of how the tools work.

Setup a piControl run
#####################

Navigate to your scratch space (ords or sitestore is good since all code is kept here) and setup a new piControl run as follows [#f1]_:

.. code-block:: bash
    
    imsi setup --repo=https://gitlab.science.gc.ca/CanESM/CanESM5.git --ver=develop_canesm --exp=cmip6-piControl --model=canesm51_p1 --runid=<unique-runid>

where you replace `--ver` and `--exp` as needed. Be sure to specify a unique runid, that won't clash with other
users, and keep it below about 15 characters. Using initials is good. e.g. `--runid=ncs-hist-tst-01`


Next change into the run directory (named for <runid>) and build the executables:

.. code-block:: bash
    
    imsi build

Save the default restart files:

.. code-block:: bash
    
    ./save_restart_files.sh

Submit the run to the batch queue:

.. code-block:: bash
    
    imsi submit

You should submit the run on the ppps!

Note as of writing, this imsi system is using a simple shell sequencer (SSS) to run the model,
not maestro (or cylc). Additional sequencing capability will be added in future. For now SSS
is being used for testing. Under SSS the model in 1 year chunks, followed by a single large
diagnostics job, which also will run in 1 year chunks.


.. rubric:: Footnotes

.. [#f1] Note, using the `https` address for gitlab is used here as it has no additional requirements. In general using `--repo=git@gitlab.com:cccma/canesm.git` is superior, but requires `adding your ssh keys to gitlab <https://docs.gitlab.com/ee/user/ssh.html>`_. 
