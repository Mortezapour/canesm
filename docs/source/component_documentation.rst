.. _component-documentation:

=======================
Component Documentation
=======================

The more detailed and technical documentation for each model component are listed here

* `CanCPL <https://cancpl.readthedocs.io>`_
* `CLASSIC documentation (may be more recent than used in CanESM) <https://cccma.gitlab.io/classic/index.html>`_
* `NEMO user manuals <https://www.nemo-ocean.eu/bibliography/how-to-cite/>`_
* CanAM scientific user guide (not currently rendered - see documentation inline with code).