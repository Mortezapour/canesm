#!/bin/bash
#
#   BUILD CI
#
#       For a setup run, this script navigates to the
#       run directory and compiles the executables using
#       compile-canesm.sh. It then checks for the necessary
#       executables and stores the compilation logs
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set -ex

# source global ci config vars
source ./tests/ci/ci.conf

# check for necessary config vars and set downstream ones
[[ -z $IDENTIFIER ]]    && { echo "IDENTIFIER must be defined"; exit 1; }
[[ -z $TEST_RUN_DIR ]]  && { echo "TEST_RUN_DIR must be defined"; exit 1; }
[[ -z $BUILD_MACH ]]    && { echo "BUILD_MACH must be defined"; exit 1; }
[[ -z $RUN_CONFIG ]]    && { echo "RUN_CONFIG must be defined"; exit 1; }
RUNID=ci-${IDENTIFIER}-${CI_COMMIT_SHA:0:7}
RUN_DIRECTORY=${TEST_RUN_DIR}/${RUNID}
OCEAN_EXECS="nemo.exe nemo_carbon-canoe_rtd.exe nemo_carbon-cmoc_rtd.exe nemo_diag.exe nemo_diag_canoe.exe nemo_diag_cmoc.exe nemo_ice_rtd.exe nemo_physical_rtd.exe rebuild_nemo.exe rebuild_nemo_mpi.exe"
AGCM_EXECS="canam.exe"
COUPLER_EXECS="cancpl.exe"
case $RUN_CONFIG in
     ESM) need_agcm=1; need_ocean=1; need_coupler=1; need_agcm_diags=1 ;;
    AMIP) need_agcm=1; need_ocean=0; need_coupler=1; need_agcm_diags=1 ;;
    OMIP) need_agcm=0; need_ocean=1; need_coupler=0; need_agcm_diags=0 ;;
esac

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# On the build machine, navigate to the run directory, 
#   source the environment, and build the executables
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo ""
echo "Comping executables for $RUNID..."
echo ""
ssh $BUILD_MACH << EOF
    cd $RUN_DIRECTORY
    source env_setup_file
    compile-canesm.sh
EOF

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# check for executables and output log if any failures occurred
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
cd $RUN_DIRECTORY
source env_setup_file 2> /dev/null
compilation_errors=0
exec_storage_dir=$(grep -Po --color=never '(?<=EXEC_STORAGE_DIR=).*' config/canesm-shell-params.sh)
function check_exec_list(){
    local exec_list=$@
    local execs_missing=0
    local true_path_exe
    for exe in $exec_list; do
        true_path_exe=$(readlink -e ${exec_storage_dir}/${exe})
        if ! [[ -e $true_path_exe ]]; then
            execs_missing=1
            echo "$exe missing!"
        fi
    done
    return $execs_missing
}

# check for execs
echo ""
echo "Checking for executables..."
echo ""
log_prefix=".compile-canesm"
if (( need_agcm == 1 )); then
    check_exec_list $AGCM_EXECS || agcm_comp_errors=1
    if (( agcm_comp_errors == 1 )); then
        echo ""
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "FAILURE IN AGCM COMPILATION!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo ""
        compilation_errors=1

        echo "Abort messages in log..."
        grep -oh "compilation aborted.*" ${log_prefix}-canam*.log
        echo ""
    fi
fi
if (( need_coupler == 1 )); then
    check_exec_list $COUPLER_EXECS || coupler_comp_errors=1
    if (( coupler_comp_errors == 1 )); then
        echo ""
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "FAILURE IN COUPLER COMPILATION!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo ""
        compilation_errors=1

        echo "Abort messages in log..."
        grep -oh "compilation aborted.*" ${log_prefix}-cancpl*.log
        echo ""
    fi
fi
if (( need_ocean == 1 )); then
    check_exec_list $OCEAN_EXECS || ocean_comp_errors=1
    if (( ocean_comp_errors == 1 )); then
        echo ""
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "FAILURE IN OCEAN COMPILAION!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo ""
        compilation_errors=1

        echo "Abort messages in log..."
        grep -oh "compilation aborted.*" ${log_prefix}-cannemo*.log
        echo ""
    fi
fi
if (( need_agcm_diags == 1 )); then
    true_agcm_diag_bin_path=$(readlink -e ${exec_storage_dir}/bin_diag)
    true_ref_agcm_diag_path=$(readlink -e $CANDIAG_REF_PATH)
    if ! [[ $true_agcm_diag_bin_path/ == $true_ref_agcm_diag_path/* ]]; then
        echo "Warning: Reference CanDIAG library not used!"

        # check for aborts in the compilation log
        if grep -q "compilation aborted" ${log_prefix}-candiag*.log; then
            echo ""
            echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            echo "FAILURE IN CANDIAG COMPILATION!"
            echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            echo ""
            compilation_errors=1

            echo "Abort messages in log..."
            grep -oh "compilation aborted.*" ${log_prefix}-candiag*.log
            echo ""
        fi
    fi
fi

# store compilation logs so they can be picked up as artifacts (cleaning stale ones if this is a retry)
log_storage_dir=${CI_PROJECT_DIR}/ci_logs_compilation_logs_${CI_JOB_NAME}_${CI_COMMIT_SHA:0:7}
rm -rf $log_storage_dir
mkdir $log_storage_dir
for f in $(ls ${log_prefix}*); do
    cp ${f} ${log_storage_dir}/${f#\.}
done

# lastly, throw non-zero exit status if anything failed
if (( compilation_errors == 1 )); then
    echo "BUILD FAILURE! Check artifacts for build logs!"
    exit 1
fi

echo ""
echo "Build Successful!"
echo ""
