# Make variables for the gnu compiler in the canesm container
#============================================================

# Fortran
FC = mpifort
FFLAGS_OPT = -O2
FFLAGS_DEBUG = -O0 -g -traceback
FFLAGS_FLOAT_CONTROL = 
FFLAGS_64BIT_REAL = -fdefault-real-8 
FFLAGS_64BIT_INT = -fdefault-integer-8
FFLAGS_MOD = -J
FFLAGS_CPP = -cpp
FFLAGS_OMP = -fopenmp
FFLAGS_NETCDF = 
FFLAGS_MPI = 
FFLAGS_STATIC = 
FFLAGS_PIC = -fPIC

# Unique/Implementation/Component Flags 
#   - Note: these are meant to allow for one off/easy additions of flags
#           for specific components. If it decided that we want to apply
#           them consistently, we make more evocative FFLAGS_* vars (as above)
FFLAGS_CANDIAG = -fconvert=big-endian -fno-range-check

# C 
CC = gcc
### add C flags as necessary
CFLAGS =

# Include Flags
INCLUDEFLAGS_USR = -I/usr/include

# Linking Flags
LDFLAGS_USR = -L/usr/lib
LDFLAGS_NETCDF = -lnetcdf -lnetcdff
LDFLAGS_CANDIAG_PLT =
