! The following variables are related to the "critical parameters" for the CanAM
! Only the scalars are listed here.  The lists, e.g., level information, are listed
! at the end of the file since they can get pretty long.

! Variables read in by ingcm
&INGCM
  ksteps=288,       ! number of time-steps between coupling cycles
  kstart=0,         ! final step counter for the previous run
  kfinal=999,       ! ending step counter for the AGCM
  agcm_rstctl=0,    ! controls how ingcm handles kstart comparison between the restart and this namelist
                    !   0 -> just outputs the input variables
                    !   1 -> if bails if there are inconsistencies
  newrun=0,
  levs1=80,         ! number of vertical moisture levels; for initialization runs (kount=0 only)

  incd=1,
  jlatpr=0,
  delt=300.0,       ! AGCM time-step
  ifdiff=0,
  icoordc="ET15",
  iyear=1850,
/

&agcm_sizes
  nlatd   = 96,         ! number of gaussian latitudes on the dynamics grid
  lonsld  = 192,        ! number of longitudes on the dynamics grid
  lond    = 192         ! number of grid points per slice for dynamics
  nlat    = 64,         ! number of gaussian latitudes on the physics grid
  lonsl   = 128,        ! number of longitudes on the physics grid
  lonp    = 128,        ! number of grid points per slice for physics
  ilev    = 80,         ! number of vertical model levels
  levs    = 80,         ! number of vertical moisture levels
  nnodex  = 32,         ! number of (smp?) nodes used to run the AGCM
  ilg     = 131
  ilgd    = 195
  lmtotal = 64
/
!  ntrac=16,             ! total number of tracers in the AGCM
!  ntraca=12,            ! number of advected tracers in the AGCM

! Other critical scalar parameters need to define the structure of CanAM
&CRIT_PARAM_CANAM_SCALAR
  lmt=63,             ! spectral truncation wave number
  lon=128,            ! number of grid points per slice for physics
  lay=2,
  coord=ET15,
  plid=.0575,
  iflm=1,             ! integer switch to control fractional land mask (1 -> on)
  nnode_a=32,         ! number of (smp?) nodes used to run the AGCM - this doesn't seem to be the right name.. it gets changed to NNODEX in the code
  NRFP=1
/

&agcm_options
  agcm_river_routing = .true.
  biogeochem = .true.
  cf_sites = .false.
  dynrhs = .false.
  isavdts = .false.
  myrssti = .false.
  parallel_io = .true.
  relax = .false.
  timers = .false.
  use_canam_32bit = .true.
  rcm = .false.
/

! Parameters for relaxation scheme
&relax_parm
      !--- Set the relaxation parameters
      relax_spc = 1             ! spectral nudging
      relax_spc_bc = 0          ! spectral climatological nudging

      relax_tg1 = 0             ! Ground temperature level 1 nudging
      relax_tg1_bc = 0          ! Ground temperature level 1 climatological nudging

      relax_wg1 = 0             ! Ground moisture level 1 nudging
      relax_wg1_bc = 0          ! Ground moisture level 1 climatological nudging

      relax_ocn = 0             ! Upper ocean (SST/SIC/SICN) nudging
      relax_ocn_bc = 0          ! Upper ocean climatological nudging

      iperiod_spc=6             ! Spectral nudging cycle in hours.
      iperiod_lnd=24            ! Land nudging cycle in hours.
      iperiod_lnd_clm=6         ! Land climatological nudging cycle in hours.
      iperiod_ocn=24            ! Ocean nudging cycle in hours.
      iperiod_ocn_clm=24        ! Ocean climatological nudging cycle in hours.

      relax_p=1.                ! VORT vorticity relaxation switch.
      relax_c=1.                ! DIV  divergence relaxation switch.
      relax_t=1.                ! TEMP temperature relaxation switch.
      relax_es=1.               ! ES   humidity relaxation switch.
      relax_ps=0.               ! PS   ln(surface pressure) relaxation switch.

      tauh_p=24.                ! VORT relaxation time scale in hours
      tauh_c=24.                ! DIV  relaxation time scale in hours
      tauh_t=24.                ! TEMP relaxation time scale in hours
      tauh_es=24.               ! HUM  relaxation time scale in hours
      tauh_ps=24.               ! PS   relaxation time scale in hours

      tauh_tg1=72.              ! TG1 relaxation time scale in hours
      tauh_wg1=72.              ! WG1 relaxation time scale in hours

      ntrunc_p=21               ! truncation of VORT tendencies
      ntrunc_c=21               ! truncation of DIV  tendencies
      ntrunc_t=21               ! truncation of TEMP tendencies
      ntrunc_es=21              ! truncation of HUM  tendencies
      ntrunc_ps=21              ! truncation of PS   tendencies

      itrunc_tnd = 1            ! itrunc_tnd - (0/1) truncate tendency with truncf
      itrunc_anl = 0            ! trunc_anl - (0/1) truncate analysis data towards which model is nudged

      ivfp=0                    ! =0, no vertical profile to nudge everywhere with equal strength
                                ! =1, use vertical profile at the top to reduce nudging near the top
                                ! =2 vertical profile at the bottom to reduce nudging near the bottom
                                ! =3 vertical profile both at the top and bottom to reduce nudging at both ends

      sgtop=0.001               ! top full levels (0.001~1hPa)
      shtop=0.001               ! top half levels (0.001~1hPa)

      sgfullt_p=0.010           ! where to start reducing forcing (0.010~10hPa)
      sgfullt_c=0.010           ! where to start reducing forcing (0.010~10hPa)
      shfullt_t=0.010           ! where to start reducing forcing (0.010~10hPa)
      shfullt_e=0.010           ! where to start reducing forcing (0.010~10hPa)

      sgdampt_p=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)
      sgdampt_c=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)
      shdampt_t=0.010           ! reduction factor from shfull to shtop (0.01=1%)
      shdampt_e=0.010           ! reduction factor from shfull to shtop (0.01=1%)

      sgbot=0.995               ! bottom full levels (0.995~995hPa)
      shbot=0.995               ! bottom half levels (0.995~995hPa)
      sgfullb_p=0.850           ! where to start reducing forcing (0.850~850hPa)
      sgfullb_c=0.850           ! where to start reducing forcing (0.850~850hPa)
      shfullb_t=0.850           ! where to start reducing forcing (0.850~850hPa)
      shfullb_e=0.850           ! where to start reducing forcing (0.850~850hPa)

      sgdampb_p=0.010           ! reduction factor from sgfulb to sgbot (0.01=1%)
      sgdampb_c=0.010           ! reduction factor from sgfulb to sgbot (0.01=1%)
      shdampb_t=0.010           ! reduction factor from shfulb to shbot (0.01=1%)
      shdampb_e=0.010           ! reduction factor from shfulb to shbot (0.01=1%)

      tauh_sst=72.              ! in hours - nudging time scale for SSTs
      tauh_edge=72.             ! in hours - nudging time scale for sea-ice mass (SIC) near edge
                                ! may use shorter time scale since it is better observed
      tauh_sic=168.             ! in hours - nudging time scale for sea-ice mass (SIC)

      rlx_sst=1.                ! convert delta sst to heat tendency
      rlx_sic=1.                ! convert delta sic to heat tendency

      rlx_tbeg=1.               ! turn on/off (1/0) tbeg heat tendency
      rlx_tbei=1.               ! turn on/off (1/0) tbei heat tendency
      rlx_tbes=1.               ! turn on/off (1/0) tbes heat tendency

      rlc_tbeg=0.               ! turn on/off (1/0) tbeg heat clim. tendency
      rlc_tbei=0.               ! turn on/off (1/0) tbei heat clim. tendency
      rlc_tbes=0.               ! turn on/off (1/0) tbes heat clim. tendency

      rlx_sbeg=1.               ! turn on/off (1/0) sbeg heat tendency
      rlx_sbei=1.               ! turn on/off (1/0) sbei heat tendency
      rlx_sbes=1.               ! turn on/off (1/0) sbes heat tendency

      rlc_sbeg=0.               ! turn on/off (1/0) sbeg heat clim. tendency
      rlc_sbei=0.               ! turn on/off (1/0) sbei heat clim. tendency
      rlc_sbes=0.               ! turn on/off (1/0) sbes heat clim. tendency
/

! Details about the vertical layer information (from the critical parameters)
!
! Momentum levels (note that some of the upper levels are encoded)
! Empty values are present since the code is writen such that there
! is current a maximum array size of 100 for the level information
&CANAM_LEVEL_MOMENTUM
  g(1)=-4778,
  g(2)=-3109,
  g(3)=-3152,
  g(4)=-3213,
  g(5)=-3298,
  g(6)=-3417,
  g(7)=-3584,
  g(8)=-3817,
  g(9)=-2114,
  g(10)=-2160,
  g(11)=-2224,
  g(12)=-2313,
  g(13)=-2439,
  g(14)=-2614,
  g(15)=-2858,
  g(16)=-1119,
  g(17)=-1165,
  g(18)=-1227,
  g(19)=-1310,
  g(20)=-1421,
  g(21)=-1567,
  g(22)=-1757,
  g(23)=-0100,
  g(24)=-0132,
  g(25)=-0173,
  g(26)=-0224,
  g(27)=-0288,
  g(28)=-0368,
  g(29)=-0467,
  g(30)=-0588,
  g(31)=-0736,
  g(32)=-0915,
  g(33)=99113,
  g(34)=99139,
  g(35)=99169,
  g(36)=99205,
  g(37)=99247,
  g(38)=99295,
  g(39)=99350,
  g(40)=99413,
  g(41)=99482,
  g(42)=99559,
  g(43)=99644,
  g(44)=99737,
  g(45)=99839,
  g(46)=99951,
  g(47)=107,
  g(48)=121,
  g(49)=136,
  g(50)=152,
  g(51)=171,
  g(52)=190,
  g(53)=212,
  g(54)=236,
  g(55)=262,
  g(56)=290,
  g(57)=321,
  g(58)=355,
  g(59)=390,
  g(60)=429,
  g(61)=469,
  g(62)=513,
  g(63)=558,
  g(64)=605,
  g(65)=652,
  g(66)=698,
  g(67)=742,
  g(68)=780,
  g(69)=813,
  g(70)=841,
  g(71)=865,
  g(72)=885,
  g(73)=902,
  g(74)=916,
  g(75)=930,
  g(76)=944,
  g(77)=958,
  g(78)=972,
  g(79)=986,
  g(80)=995,
  g(81)=0,
  g(82)=0,
  g(83)=0,
  g(84)=0,
  g(85)=0,
  g(86)=0,
  g(87)=0,
  g(88)=0,
  g(89)=0,
  g(90)=0,
  g(91)=0,
  g(92)=0,
  g(93)=0,
  g(94)=0,
  g(95)=0,
  g(96)=0,
  g(97)=0,
  g(98)=0,
  g(99)=0,
  g(100)=0,
/

! Thermodynamic levels (note that some of the upper levels are encoded)
&CANAM_LEVEL_THERMO
  h(1)=-4778,
  h(2)=-3109,
  h(3)=-3152,
  h(4)=-3213,
  h(5)=-3298,
  h(6)=-3417,
  h(7)=-3584,
  h(8)=-3817,
  h(9)=-2114,
  h(10)=-2160,
  h(11)=-2224,
  h(12)=-2313,
  h(13)=-2439,
  h(14)=-2614,
  h(15)=-2858,
  h(16)=-1119,
  h(17)=-1165,
  h(18)=-1227,
  h(19)=-1310,
  h(20)=-1421,
  h(21)=-1567,
  h(22)=-1757,
  h(23)=-0100,
  h(24)=-0132,
  h(25)=-0173,
  h(26)=-0224,
  h(27)=-0288,
  h(28)=-0368,
  h(29)=-0467,
  h(30)=-0588,
  h(31)=-0736,
  h(32)=-0915,
  h(33)=99113,
  h(34)=99139,
  h(35)=99169,
  h(36)=99205,
  h(37)=99247,
  h(38)=99295,
  h(39)=99350,
  h(40)=99413,
  h(41)=99482,
  h(42)=99559,
  h(43)=99644,
  h(44)=99737,
  h(45)=99839,
  h(46)=99951,
  h(47)=107,
  h(48)=121,
  h(49)=136,
  h(50)=152,
  h(51)=171,
  h(52)=190,
  h(53)=212,
  h(54)=236,
  h(55)=262,
  h(56)=290,
  h(57)=321,
  h(58)=355,
  h(59)=390,
  h(60)=429,
  h(61)=469,
  h(62)=513,
  h(63)=558,
  h(64)=605,
  h(65)=652,
  h(66)=698,
  h(67)=742,
  h(68)=780,
  h(69)=813,
  h(70)=841,
  h(71)=865,
  h(72)=885,
  h(73)=902,
  h(74)=916,
  h(75)=930,
  h(76)=944,
  h(77)=958,
  h(78)=972,
  h(79)=986,
  h(80)=995,
  h(81)=0,
  h(82)=0,
  h(83)=0,
  h(84)=0,
  h(85)=0,
  h(86)=0,
  h(87)=0,
  h(88)=0,
  h(89)=0,
  h(90)=0,
  h(91)=0,
  h(92)=0,
  h(93)=0,
  h(94)=0,
  h(95)=0,
  h(96)=0,
  h(97)=0,
  h(98)=0,
  h(99)=0,
  h(100)=0,
/
