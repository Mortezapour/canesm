# Clean old restarts
rm -rf OLDRS OLDTS

# Extract new restarts from incoming directory
cp -Lr AGCMRS_IN/agcmrs_OLDRS OLDRS
cp -Lr AGCMRS_IN/agcmrs_OLDTS OLDTS

# update counters
update_agcm_counters start_date=${CURRENT_YEAR}-01-01 stop_date=${CURRENT_YEAR}-12-31 agcm_timestep=900 namelist_file=modl.dat

