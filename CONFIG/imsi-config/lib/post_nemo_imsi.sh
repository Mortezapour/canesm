# useless renaming for convenctions
nemo_restart_files="ocean.output timing.output final.state ${CanNEMO_EXEC} time.step namelist namelist_top namelist_ice namelist_cmoc namelist_pisces"

for f in $nemo_restart_files; do
    rm -f rs_${f}
    mv ${f} rs_${f}
done
