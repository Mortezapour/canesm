# Clean the working directory of any existing restart files
rm -rf cpl_restart.nc

# Unpack restart files from restart bundled directories:
cp -Lr CPLRS_IN/cplrs_cpl_restart.nc cpl_restart.nc
cp -Lr CPLRS_IN/cplrs*grid* .

# Update namelist counters
update_coupler_counters start_date=${CURRENT_YEAR}-01-01 stop_date=${CURRENT_YEAR}-12-31 runid=$runid namelist_file=nl_coupler_par

