# Cleanup old restarts

# Move new restarts out of archive
if [ -d NEMORS_IN_tiled ]; then
 rm -rf NEMORS_IN
 cp -Lr NEMORS_IN_tiled/*restart*.nc .
elif [ -d NEMORS_IN ]; then
 cp -Lr NEMORS_IN/*restart*.nc .
else
 echo "NEMO restart not available at $PWD"
 exit 1
fi


# Update nemo namelists to recognize the restart input prefix?
# Untiled pattern
nemo_rs_files=`ls -1 *[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_restart.nc 2>/dev/null` || nemo_rs_files=""

if [ -z "$nemo_rs_files" ] ; then 
    # Tiled pattern
    nemo_rs_files=`ls -1 *[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_restart_[0-9][0-9][0-9][0-9].nc`
    [ -z "$nemo_rs_files" ] && bail "Could not find valid nemo restart file pattern"
fi

# This is the part including runid and end step, but not including the restart name or tile or .nc
nemo_restart_prefix=`echo $nemo_rs_files | cut -d' ' -f1 | cut -d'_' -f1-2`

# Now update the namelists to reflect this named restart
sed -i "s/.*cn_ocerst_in.*=.*/cn_ocerst_in=${nemo_restart_prefix}_restart/" namelist
sed -i "s/.*cn_icerst_in.*=.*/cn_icerst_in=${nemo_restart_prefix}_restart_ice/" namelist_ice
sed -i "s/.*cn_trcrst_in.*=.*/cn_trcrst_in=${nemo_restart_prefix}_restart_trc/" namelist_top


# update counters
update_nemo_counters start_date=${CURRENT_YEAR}-01-01 stop_date=${CURRENT_YEAR}-12-31 nemo_timestep=3600 ref_date=${START_YEAR}-01-01 namelist_file=namelist
